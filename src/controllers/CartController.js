const { response } = require('express');
const Cart = require('../models/Cart');(

//Criar uma instância nova do objeto Order no BD
const create = async(req,res) => {
    try{
            const order = await Order.create(req.body);
            return res.status(201).json({message: "Um novo pedido foi criado.", cart: cart});
        }catch(err){
            res.status(500).json({error: err});
        }
};

//Retornar todas as instâncias de um objeto Order no BD
const index = async(req,res) => {
    try {
        const order = await Order.FindAll();
        return res.status(200).json({cart});
    }catch(err){
        return res.status(500).json({err});
    }
};

//Retornar informação de uma instância específica no BD
const show = async(req,res) => {
    const{id} = req.params;
    try {
        const order = await Order.update(id);
        return res.status(200).json((cart));
    }
    catch(err) {
        res.status(500).json((error: err));
    }
};

//Editar uma instância específica no BD
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Order.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await Order.findByPk(id);
            return res.status(200).send(cart);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("O carro de compras não pode ser alterado");
    }
};

//Deletar uma instância específica no BD
const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Order.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Pedido deletado.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Pedido não encontrado.");
    }
};

    
module.exports = {
    create,
    index,
    show, 
    update,
    destroy
};