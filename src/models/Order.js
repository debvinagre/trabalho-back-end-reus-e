const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

//Order se refere no projeto BD realizado 'Compra' em inglês

const Order = sequelize.define('Order', {
    
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    adress: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING,
        allowNull: false
    },
});
Order.associate = function(models) {
    Order.hasMany(models.Cart);
};


module.exports = Order;