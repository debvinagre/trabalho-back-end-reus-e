const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

//Cart se refere no projeto BD realizado 'Cesta' de compras em inglês

const Cart = sequelize.define('Cart', {
    
    note: {
        type: DataTypes.STRING,
        allowNull: false
    },
    itens: {
        type: DataTypes.STRING,
        allowNull: false
    },
    date: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull:false
    },
    type: {
        type: DataTypes.STRING,
        allowNull:false
    }
});

Cart.associate = function(models) {
    Cart.hasMany(models.Order);
};


module.exports = Cart;